from tkinter import *

matriz = []
palabras = []
lista_botones = []
botones_presionados=[]
lista_labels = []
palabra=''
ultimo_indice = None
direccion = None

with open("sopas/letras2.txt", "r") as f:
    for linea in f:
        palabras.append(linea.strip("\n"))

with open("sopas/sopa.txt","r") as f:
    matriz = [ linea.split() for linea in f ]

def limpiar():
    global palabra
    global ultimo_indice
    global direccion
    palabra = ''
    ultimo_indice=None
    direccion=None
    for i in botones_presionados:
        btn = lista_botones[i]
        btn.configure(bg="lightgray")

def clic_boton(posicion_boton,indice_en_matriz):
    global palabra
    global ultimo_indice
    global direccion
    global botones_presionados
    
    dif_fila=0
    dif_col=0
    
    if ultimo_indice == None:
        ultimo_indice = indice_en_matriz[:]
    
    dif_fila = indice_en_matriz[0]-ultimo_indice[0]
    dif_col = indice_en_matriz[1]-ultimo_indice[1]

    if dif_fila >= -1 and dif_fila <= 1 and dif_col >= -1 and dif_col <= 1:
        
        if direccion == None and (dif_fila!=0 or dif_col!=0):
            direccion = (dif_fila,dif_col)
        
        print(direccion)
        if direccion != None:
            if dif_fila != direccion[0] or dif_col!=direccion[1]:
                return 

        btn = lista_botones[posicion_boton]
        btn.configure(bg="lightblue")
        botones_presionados.append(posicion_boton)
        palabra += btn['text']
        if palabra in palabras:
            for label in lista_labels:
                if label['text']==palabra:
                    label.configure(bg="orange")
                    palabra=''
                    ultimo_indice=None
                    direccion=None
                    botones_presionados=[]
                    return
        ultimo_indice = indice_en_matriz[:]

ventana = Tk()

indice_boton = 0
for i in range(len(matriz)):
    for j in range(len(matriz[i])):
        pos = [i,j]
        boton = Button(ventana, text=matriz[i][j], command=lambda arg1=indice_boton,arg2=pos:clic_boton(arg1,arg2), bg="lightgray")
        boton.grid(row=i, column=j,  sticky="nsew")
        lista_botones.append(boton)
        indice_boton+=1
k=0
for i in palabras:
    texto = Label(ventana, text=i)
    texto.grid(row=k, column=j+1)
    lista_labels.append(texto)
    k+=1

boton = Button(ventana, text="Limpiar", command=limpiar, bg="lightyellow", fg="blue")
boton.grid(row=0, column=j+2,  sticky="nsew")

ventana.mainloop()